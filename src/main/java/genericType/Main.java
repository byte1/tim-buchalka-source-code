package genericType;

public class Main {
    public static void main(String[] args) {

        FootbalPlayer tiberius = new FootbalPlayer("Tiberius");
        FootbalPlayer mark = new FootbalPlayer("Mark");
        FootbalPlayer anthony = new FootbalPlayer("Anthony");

        BasebalPlayer gitaMuresan = new BasebalPlayer("Ghita Muresan");
        BasebalPlayer deniseRodman = new BasebalPlayer("Denis Rodman");
        BasebalPlayer michelJordan = new BasebalPlayer("Michel Jordan");


        SoccerPlayer hagi = new SoccerPlayer("Hagi");
        SoccerPlayer gicaPopescu = new SoccerPlayer("Gica Popescu");
        SoccerPlayer petrescu = new SoccerPlayer("Pestredcu");

        SoccerPlayer iordanescu = new SoccerPlayer("Iordanescu");
        SoccerPlayer tatarusanu = new SoccerPlayer("Tatarusanu");
        SoccerPlayer chiriches = new SoccerPlayer("Chiriches");

        SoccerPlayer keseru = new SoccerPlayer("Keseru");
        SoccerPlayer nita = new SoccerPlayer("Nita");
        SoccerPlayer chipciu = new SoccerPlayer("Chipciu");

        SoccerPlayer marin = new SoccerPlayer("Marin");
        SoccerPlayer benzar = new SoccerPlayer("Benzar");
        SoccerPlayer tucudean = new SoccerPlayer("Tucudean");

        Team<FootbalPlayer> nyYankees = new Team<>("New York Yankees");
        nyYankees.addMembers(tiberius);
        nyYankees.addMembers(mark);
        nyYankees.addMembers(anthony);

        Team<BasebalPlayer> chicagoBulls = new Team<>("Chicago Bulls");
        chicagoBulls.addMembers(gitaMuresan);
        chicagoBulls.addMembers(deniseRodman);
        chicagoBulls.addMembers(michelJordan);

        Team<SoccerPlayer> fcViitorul = new Team<>("FC Viitorul");
        fcViitorul.addMembers(hagi);
        fcViitorul.addMembers(gicaPopescu);
        fcViitorul.addMembers(petrescu);

        Team<SoccerPlayer> fcSteaua = new Team<>("FC Steaua");
        fcViitorul.addMembers(iordanescu);
        fcViitorul.addMembers(tatarusanu);
        fcViitorul.addMembers(chiriches);

        Team<SoccerPlayer> fcDinamo = new Team<>("FC Dinamo");
        fcViitorul.addMembers(keseru);
        fcViitorul.addMembers(nita);
        fcViitorul.addMembers(chipciu);

        Team<SoccerPlayer> fcFarul = new Team<>("FC Farul");
        fcViitorul.addMembers(marin);
        fcViitorul.addMembers(benzar);
        fcViitorul.addMembers(tucudean);

        fcDinamo.matchResult(fcFarul, 1, 2);
        fcDinamo.matchResult(fcSteaua, 1, 1);
        fcDinamo.matchResult(fcViitorul, 2, 2);
        fcFarul.matchResult(fcSteaua, 0, 2);
        fcFarul.matchResult(fcViitorul, 1, 1);
        fcFarul.matchResult(fcDinamo, 0, 1);
        fcSteaua.matchResult(fcViitorul, 2, 1);


        System.out.println("Rankings: ");
        System.out.println(fcDinamo.getName() + ": " + fcDinamo.getRanking());
        System.out.println(fcFarul.getName() + ": " + fcFarul.getRanking());
        System.out.println(fcSteaua.getName() + ": " + fcSteaua.getRanking());

//        System.out.println(fcDinamo.compareTo(fcFarul));
//        System.out.println(fcFarul.compareTo(fcDinamo));
//        System.out.println(fcDinamo.compareTo(fcSteaua));
        System.out.println();


        System.out.println(fcViitorul.getTeamType());
        System.out.println(fcViitorul);
        System.out.println();

        Championship<Team<SoccerPlayer>> soccerPlayerTeams = new Championship<>();
        soccerPlayerTeams.add(fcDinamo);
        soccerPlayerTeams.add(fcFarul);
        soccerPlayerTeams.add(fcSteaua);
        soccerPlayerTeams.add(fcViitorul);

        soccerPlayerTeams.swowClassament();


    }
}
