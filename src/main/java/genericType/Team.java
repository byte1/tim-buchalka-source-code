package genericType;

import java.util.*;

public class Team<T extends Player> extends Championship {
    private String name;
    private int won;
    private int lose;
    private int tied;
    private int played;

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private ArrayList<T> members = new ArrayList<>();


    public boolean addMembers(T player) {
        if (members.contains(player)) {
            System.out.println("Player already in the list");
            return false;
        } else {
            members.add(player);
            System.out.println(player.getName() + " has been picked for team " + this.name);
            return true;
        }
    }

    public int numberOfPlayers() {
        //System.out.println("Number of players of " + this.getName() + " is: ");
        return this.members.size();
    }

    public void matchResult(Team<T> opponent, int ourResult, int theirResult) {

        String message;

        if (ourResult > theirResult) {
            won++;
            message = " beat ";
        } else if (ourResult == theirResult) {
            tied++;
            message = " drew with ";
        } else {
            lose++;
            message = " lose to ";
        }
        played++;
        if (opponent != null) {
            System.out.println(this.getName() + message + opponent.getName());
            opponent.matchResult(null, theirResult, ourResult);
        }
    }

    public void getNamesOfPlayers() {
        System.out.println("Name of players from " + this.getName() + ":");
        System.out.println();
        for (Player i : members) {
            System.out.println(i.getName());
        }
    }

    public int getRanking() {
        return (won * 3) + tied;
    }


    //With Comparator we can make anonimous clases as to compare by any field we want
    static Comparator<Team> comp = new Comparator<Team>() {
        @Override
        public int compare(Team o1, Team o2) {
            return o2.getRanking() - o1.getRanking();
        }
    };

    //This method allow us to compare only by a field of the object
    //Don't forget that if you use comparaTo(), you need to impllement Comparable
//    @Override
//    public int compareTo(Team<T> team) {
//        return team.getRanking() - getRanking();
//    }


    @Override
    public String toString() {
        return "Team: " + name + " ranking = " +
                getRanking();
    }

    public String getTeamType() {
        if (this.numberOfPlayers() > 0) {
            return String.valueOf((members.get(0)).getClass());
        } else {
            return "Void team";
        }
    }

}
