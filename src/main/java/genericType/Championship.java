package genericType;


import java.util.*;
import java.util.Collections;
import java.util.stream.Collectors;


public class Championship<T extends Team> {


    private List<T> teams = new ArrayList<>();


    //Trying to sort the teams by stream
//    List<T> sortedList = teams.stream()
//            .sorted( (t1, t2) -> t2.getRanking().compareTo(t1.getRanking()))
//            .collect(Collectors.toList());


    public boolean add(T team) {
        if (teams.contains(team)) {
            return false;
        }
        teams.add(team);
        return true;
    }

    public void swowClassament() {
        //If we use Comparator, we use sort() with the  2 arguments. But if we use Comparable , we use sort only with the first argument because sort() interally invoke compareTo().
        //Collections.sort(teams);
        Collections.sort(teams, Team.comp);
        for (T i : teams) {
            System.out.println(i.getName() + ": " + i.getRanking());
        }


    }
}





